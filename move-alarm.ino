#define PIR_PIN 8
#define DIODE_PIN 12

boolean moveFound = false;
long time;

void setup() {
  pinMode(PIR_PIN, INPUT);
  pinMode(DIODE_PIN, OUTPUT);
  
  Serial.begin (9600);
  time = millis();
}

void loop() {
  int sensorState = digitalRead(PIR_PIN);
  
  Serial.print("Sensor: ");
  Serial.println(sensorState);
  if (sensorState == LOW) {
    if (moveFound) {
      long newTime = millis();
      Serial.print(" ");
      Serial.print(newTime - time);
      Serial.print(" ms");
      time = newTime;
    }
    moveFound = false;
    digitalWrite(DIODE_PIN, HIGH);
  } else {
    if (!moveFound) {
      long newTime = millis();
      Serial.print(" ");
      Serial.print(newTime - time);
      Serial.println(" ms");
      time = newTime;
    }
    moveFound = true;
    digitalWrite(DIODE_PIN, LOW);
  }
  delay(100);
  
}
